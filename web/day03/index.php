<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day03</title>
    <link rel="stylesheet" href="index.css">
</head>
<body>
    <div class="center">
        <div class="container">
            <div class="wrapper">
                <div class="label-row name">
                    <button>Họ và tên</button>
                    <input type="text">
                </div>
                <div class="label-row gender">
                    <button>Giới tính</button>
                    <?php 
                        $gender = array("Nam", "Nữ");
                        for ($i = 0; $i < count($gender); $i++) {
                            echo "<div class='gender-option'>
                            <input type='radio' name='gender' id=gender-$gender[$i] value=$i>
                            <label>$gender[$i]</label>
                            </div>";
                        }
                    ?>
                </div>
                <div class="label-row department">
                    <button>Phân khoa</button>
                    <div class="select-box">
                        <input id="select-input" readonly type="text">
                        <div class="arrow-down" id="button-dropdown"></div>
                        <ul class="dropdown hide" tabindex="-1">
                            <?php
                                $departments = array(array("None", ""), array("MAT", "Khoa học máy tính"), array("KDL", "Khoa học vật liệu"));
                                foreach ($departments as $department) {
                                    echo "<li value=$department[0]>$department[1]</li>";                        
                                }
                            ?>  
                        </ul>
                    </div>
                </div>
                <div class="register">
                    <button>Đăng ký</button>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    document.addEventListener("DOMContentLoaded", () => {
        /**
         * Đóng mở dropdown bằng button
         * CreatedBy: Viet2707 - 23/09/2022
         */
        var dropdown = document.querySelector(".dropdown")
        document.querySelector("#button-dropdown").addEventListener("click", () => {
            dropdown.classList.toggle("hide")
            dropdown.focus()
        })

        /**
         * Chọn option
         * CreatedBy: Viet2707 - 23/09/2022
         */
        var options = document.querySelectorAll(".dropdown li")
        var input = document.querySelector("#select-input")
        options.forEach(option => {
            option.addEventListener("click", () => {
                var value = option.innerHTML;
                input.setAttribute("value", value)
                input.setAttribute("option", option.getAttribute("value"))
                dropdown.classList.add("hide")
            })
        })

        /**
         * Đóng dropdown khi click outside
         * CreatedBy: Viet2707 - 23/09/2022
         */
        dropdown.addEventListener("blur", () => {
            dropdown.classList.add("hide")
        })
    });
</script>
</html>